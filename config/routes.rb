Rails.application.routes.draw do
  get 'home' => 'page#home'
  get 'signup' => 'page#signup'
  get 'login' => 'page#login'
  get 'profile' => 'page#profile'
  get 'postproject' => 'page#postproject'
  get 'newrequest' => 'page#newrequest'
  get 'whatshot' => 'page#whatshot'
  get 'about' => 'page#about'
  get 'newmessage' => 'page#newmessage'
  get 'userinbox' => 'page#userinbox'
  get 'viewproject' => 'page#viewproject'

  root 'page#home'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
